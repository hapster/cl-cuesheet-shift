#+TITLE: CUESHEET-SHIFT: Forget the audio-cuesheet mismatch in your Mixxxes!

* Why and what

If you use the free DJ software [[https://mixxx.org][Mixxx]] like I do, it is possible that a recording starts
earlier than the first song's playback.  This gap reproduces when you shorten the mix for
uploading but your cuesheet remains the same, and makes it hard for listeners to know
which song plays when.

Using this program the index position of songs in cuesheets can be shifted to close that
gap and to align the start of the recording and the start of your first song.

* Status / Issues

The project is in beta state.  If you know exactly what you are doing, the program does
what you want.  If you don't know what you are doing, you are certain to run into error
messages that won't help you find the source of the error.  Furthermore, the code quality
is congruent with the author's abilities, which means that because I am a noob, it might
not be a good learning resource for others.

* Requirements

To run the program, you will need a [[https://lisp-lang.org][Common Lisp]] implementation.  The program is known to
work with [[https://www.sbcl.org][SBCL]] (v2.3.3).

The repository needs to be in a place that the system distribution facility [[https://gitlab.common-lisp.net/asdf/asdf/][ASDF]] can see.
For information on how to make sure that's the case, [[https://asdf.common-lisp.dev/asdf.html#Configuring-ASDF-to-find-your-systems][see here]].

Finally, you need [[https://www.quicklisp.org/beta/][Quicklisp]], a program to make sure that the required libraries are
downloaded.  The link above also contains an explanation on how to install it.

* Usage

If you are not familiar with one of the classical IDE/REPL environments such as [[https://www.gnu.org/software/emacs/][Emacs]] with
[[https://github.com/joaotavora/sly][SLY]], you can use ~sbcl~ in your preferred terminal emulator to start a REPL.

In the REPL, ~(ql:quickload :hapster.cuesheet-shift)~ loads the project and its dependencies
into the Lisp environment (if this doesn't work you likely need to check if ASDF is
properly configured or your repository properly placed).

~(cue-shift:cuesheet-shift "~/path/to/your/sheet.cue" :min X :sec Y :msec Z)~ is the main
operation provided by this package.  Providing a valid file is mandatory, while the
parameters `:min`, `:sec` and `:msec` are optional (omitting them will rewrite the file
without altering the timings).  Positive integers will shift the songs' index position
backwards (i.e. "earlier"), negative integers will shift the songs' index position
forwards (i.e. "later").

**NOTE**: Your parameters must be smaller than the start of your first song.  If your
first song starts at 00:03:00 (0 Minutes, 3 Seconds, 0 Milliseconds) and you choose
parameters that result in a negative time like -01:03:00 (~:min 1~), the program will
tell you that "the stack is exhausted".
