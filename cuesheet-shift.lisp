(in-package :hapster.cuesheet-shift)

(defun prune-time (msec)
  "Constrain MSEC to the first two digits."
  (if (>= msec 100)
      (parse-integer (subseq (format nil "~d" msec) 0 2))
    msec))

(defun positive-time-p (min sec msec)
  "Return T if MIN, SEC and MSEC are either positive and can substitute one another, else
NIL."
  (not (or (and (< min 0)
		(< sec (* (* -1 min) 60)))
	   (and (< sec 0)
		(< msec (* (* -1 sec) 1000)))
	   (< msec 0))))

(defun rollover-time (min sec msec)
  "Rolls over excessive values for MIN, SEC and MSEC."
  (cond ((>= msec 1000)
	 (rollover-time min (1+ sec) (- msec 1000)))
	((< msec 0)
	 (rollover-time min (1- sec) (+ msec 1000)))
	((>= sec 60)
	 (rollover-time (1+ min) (- sec 60) msec))
	((and (< sec 0)
	      (> min 0))
	 (rollover-time (1- min) (+ sec 60) msec))
	(t (list min sec msec))))

(defun index-transpose-time (str &key (min 0) (sec 0) (msec 0))
  "Given STR and MIN, SEC and/or MSEC, transpose times.  Return new-times, INDEX-str,
INDEX-track-no and old-times."
  (let* ((index-split (str:split " " str))
	 (index-strlist (str:split ":" (first (last index-split))))
	 (index-numlist (mapcar #'parse-integer index-strlist))
	 (index-rest (nthcdr 4 (butlast index-split))))
    (destructuring-bind (index-min index-sec index-msec)
	index-numlist
      (list (rollover-time (- index-min min)
			   (- index-sec sec)
			   (- index-msec msec))
	    index-rest
	    (list index-min index-sec index-msec)))))

(defun produce-time-string (min sec msec)
  "Given MIN, SEC and MSEC, return STRING of format \"XX:YY:ZZ\"."
  (format nil "~2,'0d:~2,'0d:~2,'0d" min sec msec))

(defun index-shift-time (str &key (min 0) (sec 0) (msec 0))
  "Given an \"INDEX\" STR of one song of a cuesheet string-list, shift its index
  position according to the shift parameters (MIN, SEC) and return the string."
  (destructuring-bind
      ((new-min new-sec new-msec) (index-str track-no-str) (old-min old-sec old-msec))
      (index-transpose-time str :min min :sec sec :msec msec)
    (if (or (> (parse-integer track-no-str) 1)
	    (positive-time-p new-min new-sec new-msec))
	(str:join " "
		  (list "   "
			index-str
			track-no-str
			(produce-time-string new-min new-sec (prune-time new-msec))))
	(error "Resulting values are negative.  Maximum shiftable values are ~S
  MIN, ~S SEC and ~S MSEC" old-min old-sec old-msec))
    ))

(defun songlist-shift-times (str-list &key (min 0) (sec 0) (msec 0))
  "Given the cuesheet STR-LIST, move the songs' index position according to SHIFT and
return a string.

Utility function to be called by CUESHEET-SHIFT and not to be used on its own."  
  (cond ((null str-list) nil)
	((str:containsp "INDEX" (car str-list))
	 (cons (index-shift-time (car str-list) :min min :sec sec :msec msec)
	       (songlist-shift-times (cdr str-list) :min min :sec sec :msec msec)))
	(t (cons (car str-list)
		 (songlist-shift-times (cdr str-list)
				       :min min
				       :sec sec
				       :msec msec)))))

(defun cuesheet-shift (sheet &key (min 0) (sec 0) (msec 0))
  "In SHEET, offset the index position of songs by shift parameters (MIN, SEC, MSEC)."
  (with-open-file (stream sheet :direction :output
				:if-exists :overwrite
				:if-does-not-exist :error)
    (let ((cue-strlist (uiop:read-file-lines sheet)))
      (dolist (i (songlist-shift-times cue-strlist
				       :min min
				       :sec sec
				       :msec msec))
	(format stream "~&~a" i)))))
