(asdf:defsystem #:hapster.cuesheet-shift
  :version "0.0.1"
  :description "Shift songs' index position in cue sheets"
  :serial t
  :license "GPLv3"
  :components
  ((:file "packages")
   (:file "cuesheet-shift" :depends-on ("packages")))
  :depends-on (:str))
